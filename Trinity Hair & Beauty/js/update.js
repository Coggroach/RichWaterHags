function GetCurrentDate()
{
	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	if(month.toString().length < 2) { 
		month = '0' + month;
	}
	if(day.toString().length < 2) { 
		day = '0' + day;
	}
	return year + '-' + month + '-' + day;
}

function GetCurrentTime()
{
	var d = new Date();
	var hour = d.getHours();
	var mins = d.getMinutes();
	if(hour.toString().length < 2) { 
		hour = '0' + hour;
	}
	if(mins.toString().length < 2) { 
		mins = '0' + mins;
	}
	return hour + ':' + mins;
}

$(document).ready(function(){
	var elem = document.getElementById("reservationDate");
	elem.value = GetCurrentDate();
	
	elem = document.getElementById("reservationTime");
	elem.value = GetCurrentTime();
	
	$("#reservationButton").on('click', function(event){
		var contactName = document.getElementById("contactName");
		var contactDate = document.getElementById("contactDate");
		var contactTitle = document.getElementById("contactTitle");
		var name = document.getElementById("reservationName");
		var phone = document.getElementById("reservationPhone");
		var date = document.getElementById("reservationDate");
		
		if(name.value != "" && phone.value != "")
		{
			contactTitle.innerText = "Reservation Success";
			contactName.innerText = name.value;
			contactDate.innerText = date.value;
		} else {
			contactTitle.innerText = "Reservation Error";
			
		}
	});
});

