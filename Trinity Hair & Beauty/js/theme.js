function myMap() 
{
	var myCenter = new google.maps.LatLng(53.3452135, -6.2542246);
	var mapProp = {center:myCenter, zoom:18, scrollwheel:false, draggable:false, mapTypeId:google.maps.MapTypeId.ROADMAP};
	var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
	var marker = new google.maps.Marker({position:myCenter});
	marker.setMap(map);
}

function OpenOpenHours(w) 
{
    document.getElementById("openHours").style.width = w;
}

function CloseOpenHours() 
{
    document.getElementById("openHours").style.width = "0";
}

$(document).ready(function(){
  // Add smooth scrolling to all links in navbar link
  $(".navbar a").on('click', function(event) {    
    if (this.hash !== "") {      
      //event.preventDefault();
      var hash = this.hash;      
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
        window.location.hash = hash;
      });
    }
  });
  
  // Add smooth scrolling to footer link
  $("#toWelcome").on('click', function(event) { 
	if (this.hash !== "") {
		event.preventDefault();		
		$('html, body').animate({
			scrollTop: 0
		}, 900, function() {
			window.location.hash = '#';
		});
	}
  });

})