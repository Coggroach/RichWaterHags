function ServiceNavigation(event)
{
	var names= ["shop", "hair", "jfm", "face", "nails", "body"];
	var hash = window.location.hash.substring(1);
	var tabs = [];
	
	var index = parseInt(hash);
	console.log(index);
	console.log(hash);
	if(isNaN(index) || index >= names.length) {
		return;
	}
	
	for(i = 0; i < names.length; i++) {
		tabs.push(document.getElementById(names[i]));
	}	
	for(i = 0; i < names.length; i++) {
		tabs[i].classList.remove("active", "in");
	}
	tabs[index].classList.add("active", "in");
}

$(document).ready(function(){
	ServiceNavigation("");
	
	$(window).on('hashchange', ServiceNavigation);
});